const socket = new WebSocket('ws://localhost:3000/idealfall1');

const main = document.querySelector('main');
// Connection opened
socket.addEventListener('open', function (event) {
    socket.send('Hello Server!');
});

// Listen for messages
socket.addEventListener('message', function (event) {
    console.log('Message from server ', event.data);
    addNewMessage(JSON.parse(event.data))
});

let testobject = {
    content: 'Heyyy Matthias die demo geht',
    platform: 'Amy'
}

// addNewMessage(testobject);

function createElement(string) {
    let temp = document.createElement('template');
    temp.innerHTML = string;
    return temp.content;
}

function addNewMessage(message) {
    const date = new Date();
    if (message.hidden)
        main.appendChild(createElement(`<p><s>${message.content}</s> <small>${date.getHours()}:${date.getSeconds()} - ${message.platform}</small></p> `));
    else
        main.appendChild(createElement(`<p>${message.content} <small>${date.getHours()}:${date.getSeconds()} - ${message.platform}</small></p> `));
}